window.onload = function () {
    const NEXT_DATE = 1591282800000;
    const EL = document.getElementById('pubg-countdown');

    updateCountdown();
    
    setInterval(function () {
        updateCountdown()
    }, 1000)

    function updateCountdown () {
        let DAYS_LEFT, HOURS_LEFT, MINUTES_LEFT, SECONDS_LEFT = 0;
        const CURRENT_DATE = new Date().getTime();

        let TIME_LEFT = NEXT_DATE - CURRENT_DATE;

        DAYS_LEFT = Math.floor(TIME_LEFT / (1000 * 60 * 60 * 24));
        HOURS_LEFT = Math.floor((TIME_LEFT % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        MINUTES_LEFT = Math.floor((TIME_LEFT % (1000 * 60 * 60)) / (1000 * 60));
        SECONDS_LEFT = Math.floor((TIME_LEFT % (1000 * 60)) / 1000);

        EL.innerText = TIME_LEFT < 0 ? 'Currently playing' : `${DAYS_LEFT}d ${HOURS_LEFT}h ${MINUTES_LEFT}m ${SECONDS_LEFT}s`;
    }

}